# Bash Windows Backup
Bash script to save Windows users directories.
This will save users datas, chrome and firefox profiles. %APPDATA% or others Windows related will NOT be saved. I use this because I dislikes Windows tools.

## How to use it
`./windowsBackup.sh archive_name source destination`

| Parameters	| Description                                                                                                  |
| ------------- | ------------------------------------------------------------------------------------------------------------ |
| archive_name	| How you want to name the archive. I usaly use the computer's name                                            |
| source 	| What is the data source path you want to save. I usaly enter the windows mount point in GNU/linux filesystem |
| destiantion	| Where you want the archive to be saved.                                                                      |

**Note that the date will be added to archive name.**

## Improvement ideas
* automount Windows partition
* Automaticly save archive to a samba share
* Possiblity to toggle verbose mode
* sort data from files extention
